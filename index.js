module.exports = inherits;

function inherits (a, b) {
  for (var k in b.prototype) {
    a[k] = b.prototype[k];
  }
  return a;
}